## Тренировка по RecyclerView
#### данный тестовый проект, залит на сервис GitLab в качестве пробной работы с данным сервером
#### Данный проект взят с видеоурока канала [Neco Ru](https://www.youtube.com/channel/UCPz3xmUpIbo8jooCtV_vMNw)
#### MainActivity
###### [Main](https://gitlab.com/mcmouse88/plantslist/-/blob/main/app/src/main/java/com/example/recyclerviewplants/MainActivity.kt)
#### Plant
###### [Plant](https://gitlab.com/mcmouse88/plantslist/-/blob/main/app/src/main/java/com/example/recyclerviewplants/Plant.kt)
#### PlantAdapter
###### [PlantAdapter](https://gitlab.com/mcmouse88/plantslist/-/blob/main/app/src/main/java/com/example/recyclerviewplants/PlantAdapter.kt)
#### EditActivity
###### [EditActivity](https://gitlab.com/mcmouse88/plantslist/-/blob/main/app/src/main/java/com/example/recyclerviewplants/EditActivity.kt)

