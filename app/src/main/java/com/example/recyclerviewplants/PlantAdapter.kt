package com.example.recyclerviewplants

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerviewplants.databinding.PlantItemBinding
import java.util.ArrayList

class PlantAdapter : RecyclerView.Adapter<PlantAdapter.PlantHolder>() { // Адаптер для RecyclerView
    private val plantList = ArrayList<Plant>()

    class PlantHolder(item: View) : RecyclerView.ViewHolder(item) {
        private val binding = PlantItemBinding.bind(item)
        fun bind(plant: Plant) = with(binding) {
            imagePlant.setImageResource(plant.imageID)
            textViewTitle.text = plant.title
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlantHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.plant_item, parent, false)
        return PlantHolder(view)
    }

    override fun onBindViewHolder(holder: PlantHolder, position: Int) {
        holder.bind(plantList[position])
    }

    override fun getItemCount(): Int {
        return plantList.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun addPlant(plant: Plant) {
        plantList.add(plant)
        notifyDataSetChanged()
    }


}

