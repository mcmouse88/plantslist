package com.example.recyclerviewplants

import java.io.Serializable

data class Plant(val imageID: Int, val title: String, val description: String) : Serializable {

}
